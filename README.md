# Java Spring Boot Project

> File Structure:

    - Controller
        - Eg. StudentConroller
    - Model
        -Eg. Student
    - Repository
        -Eg. StudentRepository
    - Services
        -Eg. StudentService
        -Eg.StudentServiceImpl
        
## Services:
It's an iterface whereby the methods that are to be performed are intialized before they are implemented

***Code Sample***
```
public interface StudentService {

    List<Student> findAll();
    Student findById(Long id);

    void deleteStudent(Long id);

    Student createStudent(Student student);

    Student createStudent(Long id, Student student);

    Student updateStudent(Long id,Student student);

    Student updateStudent(Student student);
}
```

After the service file has been created an implementation of the file is generated. In this file is where the various mwthods will be fully implemented.

***Code Sample***
```
@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Student findById(Long id) {

        return studentRepository.findById(id).orElseThrow(()-> new NotFoundException("No university with id" +id+ "found"));
    }

    @Override
    public Student updateStudent(Student student) {
        Student found = findById(student.getId());
        found.setFirstName(student.getFirstName());
        found.setLastName(student.getLastName());
        found.setMiddleName(student.getMiddleName());
        found.setDob(student.getDob());
        return studentRepository.save(found);
    }
}
```
> @Service annotation

This is used to show that the file is intended for the service implementation.

## Repositories:
This file extends JpaRepository.
Jpa Repository contains a list of predefined functions that can be used. Eg: findAll(), saveAll(), findAllById()
***Code Sample***
```
import org.springframework.data.jpa.repository.JpaRepository;
public interface StudentRepository extends JpaRepository<Student, Long> {
}
```

## Models:
This is a class that that maps to the data tables and relationships between the tables.
The table column items are created as variables, a constructor is created and getters and setters are created.

***Code Sample***
```
@Entity
@Table(name = "students")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "dob")
    private String dob;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "university_id")
    private University university;

    public Student() {
    }

    public Student(String firstName, String lastName, University university) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.university = university;
    }
}
```
> @Entity

> @Table

> @GeneratedValue

> @Id

> @Column

> @ManyToOne

> @JsonIgnore

> @JoinColumn

## Controllers:
 This file connects the views to the the model.
 The endpoints are also written here.
 
 ***Code Sample***
 ```
@RestController
@RequestMapping(value = "students")
public class StudentController {

    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public List<Student> findAll(){
        return studentService.findAll();
    }

    @GetMapping(value = "{id}")
    public Student findById(@PathVariable Long id){

        return studentService.findById(id);
    }

    @PostMapping
    public Student createStudent(@RequestBody Student student)
    {
        return studentService.createStudent(student);
    }

    @DeleteMapping(value = "{id}")
    public void deleteStudent(@PathVariable Long id)
    {

        studentService.deleteStudent(id);
    }

    @PatchMapping(value="{id}")
    public Student updateStudent(@PathVariable Long id, @RequestBody Student student)
    {
        return studentService.updateStudent(id, student);
    }
}
 ```
 
 > @RestController
 
 > @RequestMapping
 
 > @Post/ Get/ Patch Mappings
 
 

