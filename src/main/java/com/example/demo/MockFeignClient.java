package com.example.demo;

import com.example.demo.models.Match;
import com.example.demo.models.MockAppointments;
import com.example.demo.models.MockLecturer;
import com.example.demo.models.MockStudent;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name="mockclient", url="http://10.51.10.111:2200")
public interface MockFeignClient {

    @RequestMapping(method= RequestMethod.POST, value="students")
    MockStudent createStudent(@RequestBody MockStudent mockStudent);

    @RequestMapping(method = RequestMethod.POST, value = "matches")
    Match RequestDate(@RequestParam("gender") String gender, @RequestParam("studentId") Long studentId);

    @RequestMapping(method = RequestMethod.PATCH, value = "matches/{BlindDateID}")
    Match rejectDate(@PathVariable("BlindDateID") Long blindDate, @RequestBody Match match);

    @RequestMapping(method = RequestMethod.PUT, value = "matches/{BlindDateID}")
    Match requestOtherDate(@PathVariable("BlindDateID") Long blindDate, @RequestParam("matchId") Long matchId, @RequestParam("studentId") Long studentId);




}
