package com.example.demo.models;

public class MockLecturer {

    private Long id;
    private String name;

    public MockLecturer (){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MockLecturer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
