package com.example.demo.models;

public class Match {
    private Long id;
    private Long studentId;
    private String reason;

    public Match(Long studentId, String reason) {
        this.studentId = studentId;
        this.reason = reason;
    }
    private Match(){}

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Match{" +
                "id=" + id +
                ", studentId=" + studentId +
                ", reason='" + reason + '\'' +
                '}';
    }
}
