package com.example.demo.services;

import com.example.demo.models.Student;

import java.util.List;

public interface StudentService {

    List<Student> findAll();
    Student findById(Long id);

    void deleteStudent(Long id);

    Student createStudent(Student student);

    Student createStudent(Long id, Student student);

    Student updateStudent(Long id,Student student);

    Student updateStudent(Student student);
}
