package com.example.demo.services;

import com.example.demo.models.Student;
import com.example.demo.models.University;

import java.util.List;

public interface UniversityService {

    List<University> findAll();
    University findById(Long id);

    void delete(Long id);

    University createUniversity(University university);

    University updateUniversity(University university);

    University updateUniversity(Long id,University university);

    Student createStudent(Long id, Student student);
}
