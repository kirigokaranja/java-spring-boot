package com.example.demo.services;

import com.example.demo.NotFoundException;
import com.example.demo.models.Student;
import com.example.demo.repositories.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Student findById(Long id) {

        return studentRepository.findById(id).orElseThrow(()-> new NotFoundException("No university with id" +id+ "found"));
    }

    @Override
    public void deleteStudent(Long id) {
        studentRepository.deleteById(id);
    }

    @Override
    public Student createStudent(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public Student createStudent(Long id, Student student) {
        return null;
    }

    @Override
    public Student updateStudent(Long id, Student student) {
        return null;
    }

    @Override
    public Student updateStudent(Student student) {
        Student found = findById(student.getId());
        found.setFirstName(student.getFirstName());
        found.setLastName(student.getLastName());
        found.setMiddleName(student.getMiddleName());
        found.setDob(student.getDob());
        return studentRepository.save(found);
    }
}
