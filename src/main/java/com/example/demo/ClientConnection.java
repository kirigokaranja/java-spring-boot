package com.example.demo;

import com.example.demo.models.University;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class ClientConnection implements CommandLineRunner {

    private final FeignRestClient feignrestclient;

    public ClientConnection(FeignRestClient feignrestclient) {
        this.feignrestclient = feignrestclient;
    }


    @Override
    public void run(String... args) throws Exception {
        RestTemplate restTemplate = new RestTemplate();

//        List<University> universities =  feignrestclient.getAllUniversities();
//        System.out.println("All Universities: "+ universities.toString());
//
//        University createdUniversity = feignrestclient.createUniversity(new University("Maasai Mara", "Narok"));
//        System.out.println("University added: "+ createdUniversity.toString());
//
//        University uni = feignrestclient.findUniversityById(createdUniversity.getId());
////       University uni = feignrestclient.findUniversityById((long) 5);
//        System.out.println("My University: "+ uni.toString());
//
//        University newuni = new University("Maasai Mara","Mara");
//        newuni = feignrestclient.updateUniversity(createdUniversity.getId(), newuni);
//        System.out.print("Updated University: "+ newuni.toString());
//
//        List<University> searched = feignrestclient.searchByName(createdUniversity.getName());
//        System.out.println("Searched University: "+ searched.toString());



    }
}
