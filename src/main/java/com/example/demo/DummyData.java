package com.example.demo;

import com.example.demo.models.Student;
import com.example.demo.models.University;
import com.example.demo.models.Course;
import com.example.demo.repositories.CourseRepository;
import com.example.demo.repositories.StudentRepository;
import com.example.demo.repositories.UniversityRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DummyData implements CommandLineRunner {

    private final StudentRepository studentRepository;
    private final UniversityRepository universityRepository;
    private final CourseRepository courseRepository;

    public DummyData(StudentRepository studentRepository, UniversityRepository universityRepository, CourseRepository courseRepository) {
        this.studentRepository = studentRepository;
        this.universityRepository = universityRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public void run(String... args) throws Exception {
//        Student me = new Student("Karanja", "Sharon");
//        studentRepository.save(me);

        University strath = new University("Strathmore", "Ole Sangale" );
        universityRepository.save(strath);

        University jkuat = new University("JKUAT", "jUJA");
        universityRepository.save(jkuat);

        Course course = new Course("Math 101");
        Course savedCourse = courseRepository.save(course);

        Course course1 = courseRepository.save(new Course("Architecture 101"));

        Course course2 = courseRepository.save(new Course("Human Computer Interaction"));

        Course course3 = courseRepository.save(new Course("Machine Learning"));

        Student student = studentRepository.save(new Student("Karanja", "Kirigo",  strath));

        course.addStudent(student);
        courseRepository.save(savedCourse);

        course1.addStudent(student);
        courseRepository.save(course1);

//        Student student1 = studentRepository.save(new Student("James", "Bond", strath));
//
//        course.addStudent(student1);
//        courseRepository.save(savedCourse);
//
//        course1.addStudent(student1);
//        courseRepository.save(course1);

        Student student2 = studentRepository.save(new Student("Nikita", "Jones", strath));

        course3.addStudent(student2);
        courseRepository.save(course3);

        course2.addStudent(student2);
        courseRepository.save(course2);

    }
}
