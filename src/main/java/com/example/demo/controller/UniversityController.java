package com.example.demo.controller;

import com.example.demo.models.Student;
import com.example.demo.models.University;
import com.example.demo.services.UniversityService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "universities")
public class UniversityController {

    private final UniversityService universityService;

    public UniversityController(UniversityService universityService) {
        this.universityService = universityService;
    }

    @GetMapping
    public List<University> findAll(){
        return universityService.findAll();
    }

    @GetMapping(value = "{id}")
    public University findById(@PathVariable Long id){

        return universityService.findById(id);
    }

    @PostMapping
    public University createUniversity(@RequestBody University university)
    {
        return universityService.createUniversity(university);
    }

    @DeleteMapping(value = "{id}")
    public void deleteUniversity(@PathVariable Long id)
    {
        universityService.delete(id);
    }

    @PatchMapping
    public University updateUniversity(@RequestBody University university)
    {
        return universityService.updateUniversity(university);
    }

    @PatchMapping(value="{id}")
    public University updateUniversity(@PathVariable Long id, @RequestBody University university)
    {
        return universityService.updateUniversity(id, university);
    }

    @PostMapping(value="{id}/students")
    public Student createStudent(@PathVariable Long id, @RequestBody Student student){
        return universityService.createStudent(id, student);
    }


}
