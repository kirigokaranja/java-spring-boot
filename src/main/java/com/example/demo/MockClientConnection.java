package com.example.demo;

import com.example.demo.models.Match;
import com.example.demo.models.MockAppointments;
import com.example.demo.models.MockLecturer;
import com.example.demo.models.MockStudent;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MockClientConnection implements CommandLineRunner {

    private final MockFeignClient mockFeignClient;

    public MockClientConnection(MockFeignClient mockFeignClient) {
        this.mockFeignClient = mockFeignClient;
    }

    @Override
    public void run(String... args) throws Exception {

        MockStudent student = mockFeignClient.createStudent(new MockStudent("95006", "Sharon"));
        System.out.println("Student added: "+ student);

        Match newMatch = mockFeignClient.RequestDate("MALE", student.getid());
        System.out.println("Match added: "+ newMatch);

        Match rejectMatch = mockFeignClient.rejectDate(newMatch.getId(), new Match(student.getid(), "I do not like the match"));
        Match request = mockFeignClient.requestOtherDate(newMatch.getId(),(long)20,(long)62);

//        MockStudent getStudent = mockFeignClient.getById(student.getStudentNumber());
//        System.out.println("Me:" + getStudent);
//
//        List<MockLecturer> allLecturers = mockFeignClient.getAllLecturers();
//        System.out.println("All Lecturers:" +allLecturers);
//
//        MockAppointments appointment = mockFeignClient.createAppointment(new MockAppointments(student.getid(), (long)2));
//        System.out.println("Appointment added: "+ appointment);
//
//        MockAppointments confirmAppointment = mockFeignClient.confirmAppointment(student.getid(), appointment.getId());
//        System.out.println("Appointment confirmed: "+ confirmAppointment);
    }
}
